<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserLoanWeeklyPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_loan_weekly_payments', function (Blueprint $table) {
            $table->bigIncrements('weekely_payment_id');
            $table->bigInteger('loan_id');
            $table->float('weekly_payment')->default(0);
            $table->boolean('weekly_payment_status')->default(0);
            $table->date('scheduled_payment_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_loan_weekly_payments');
    }
}
