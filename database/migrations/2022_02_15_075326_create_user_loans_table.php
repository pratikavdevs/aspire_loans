<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_loans', function (Blueprint $table) {
            $table->bigIncrements('loan_id');
            $table->integer('user_id')->index();
            $table->bigInteger('loan_amount')->default(0);
            $table->integer('loan_term')->default(0);
            $table->tinyInteger('loan_status')->default(0);
            $table->boolean('loan_weekly_repayment')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_loans');
    }
}
