# README #

### What is this repository for? ###

<p align="center">
It is an app that allows authenticated users to go through a loan application.
</p>
<p align="center">
All the loans will be assumed to have a “weekly” repayment frequency.
</p>
<p align="center">
After the loan is approved, the user must be able to submit the weekly loan repayments.
</p>
<p align="center">
It can be a simplified repay functionality, which won’t need to check if the dates are correct but will just set the weekly amount to be repaid.
</p>

### How do I get set up? ###

<p align="center">
Clone this repository
</p>
<p align="center">
Create your `.env` file and set your database credentials and database name into it
</p>
<p align="center">
Run `composer update --no-scripts` | `composer dump-autoload` (Run either of this command) to setup vendor folder
</p>
<p align="center">
Run `php artisan migrate` command to have all database tables used for project
</p>
<p align="center">
Run `php artisan passport:install` command to have token based authentication used for project
</p>
<p align="center">
Start your laravel server to access this project (`php artisan serve`)
</p>
<p align="center">
Clear config cache to have latest changes by `php artisan config:cache` command
</p>

### App Flow ###

<p align="center">
    <b>1) User registration:</b>
    <p>Route: {{server_name}}/api/register</p>
    <p>Method: POST</p>
    <p>Request: name, email & password</p>
    <p>Response: Auth Token</p>
</p>
<p align="center">
    <b>2) User login:</b>
    <p>Route: {{server_name}}/api/login</p>
    <p>Method: POST</p>
    <p>Request: email & password</p>
    <p>Authorization: Auth token</p>
    <p>Response: Auth Token</p>
</p>
<p align="center">
    <b>3) User loan application:</b>
    <p>Route: {{server_name}}/api/loan-application</p>
    <p>Method: POST</p>
    <p>Request: loan_amount & loan_term</p>
    <p>Authorization: Auth token</p>
    <p>Response: Success message</p>
</p>
<p align="center">
    <b>4) User loan application approval:</b>
    <p>Route: {{server_name}}/api/loan-application-approval</p>
    <p>Method: POST</p>
    <p>Request: loan_id</p>
    <p>Authorization: Auth token</p>
    <p>Response: Success message</p>
</p>
<p align="center">
    <b>5) Get user weekly loan repayments:</b>
    <p>Route: {{server_name}}/api/submit-weekly-repayments</p>
    <p>Method: POST</p>
    <p>Request: loan_id</p>
    <p>Authorization: Auth token</p>
    <p>Response: List of scheduled weekly repayments data</p>
</p>
<p align="center">
    <b>6) Cron to settle weekly repayments:</b>
    <p>Cron: aspire:loan-weekly-repayments</p>
    <p>Params: date (optional)</p>
</p>