<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PassportAuthController;
use App\Http\Controllers\UserLoanController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Route for User Registration
Route::post('register', [PassportAuthController::class, 'register']);

// Route for User Login
Route::post('login', [PassportAuthController::class, 'login']);

// Auth middleware
Route::middleware('auth:api')->group(function () {

    // User loan application
    Route::post('loan-application', [UserLoanController::class, 'userLoanApplication']);

    // User loan application get approved
    Route::post('loan-application-approval', [UserLoanController::class, 'userLoanApplicationApproval']);
    // User loan application get rejected
    Route::post('loan-application-reject', [UserLoanController::class, 'userLoanApplicationRejected']);

    // User allowed for weekly repayments
    Route::post('submit-weekly-repayments', [UserLoanController::class, 'userSubmitWeeklyRepayments']);
});