<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function ApiResponseError($data, $message, $code,$error=array()) {
        $responseArr = ['response_code' => $code, 'success' => false, 'message' => $message,'error'=>$error, 'data' => $data];
        return response($responseArr, $code);
    }

    public function ApiResponseSuccess($data, $message, $code) {
        $responseArr = ['response_code' => $code, 'success'=>true, 'message'=>$message, 'data' => $data ];
        return response($responseArr, $code);
    }
}
