<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserLoan;
use App\Models\UserLoanWeeklyPayment;
use Carbon\Carbon;

class UserLoanController extends Controller
{   
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @define Loan application of user
     */
    public function userLoanApplication(Request $request) {
        try {
            $request->validate([
                'loan_amount' => 'required|numeric',
                'loan_term' => 'required|numeric',
            ]);
        } catch (\Exception $e) {
            return $this->ApiResponseError([], $e->getMessage(), 400);
        }
        try {
            $user_id = auth('api')->user()->id;
            $loan_amount = $request->loan_amount;
            $loan_term = $request->loan_term;

            UserLoan::applicationForLoan($user_id, $loan_amount, $loan_term);

            return $this->ApiResponseSuccess([], 'User has applied for loan.', 200);
        } Catch (\Exception $e) {
            return $this->ApiResponseError([], $e->getMessage(), 500);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @define Loan application rejected
     */
    
    public function userLoanApplicationRejected(Request $request) {
        try {
            $request->validate([
                'loan_id' => 'required|numeric',
            ]);
        } catch (\Exception $e) {
            return $this->ApiResponseError([], $e->getMessage(), 400);
        }
        try {
            // $user_id = auth('api')->user()->id;
            $loan_id = $request->loan_id;

            UserLoan::loanApplicationStatus($loan_id, 2);

            return $this->ApiResponseSuccess([], 'User loan application is rejected.', 200);
        } Catch (\Exception $e) {
            return $this->ApiResponseError([], $e->getMessage(), 500);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @define Loan application approved
     */
    
    public function userLoanApplicationApproval(Request $request) {
        try {
            $request->validate([
                'loan_id' => 'required|numeric',
            ]);
        } catch (\Exception $e) {
            return $this->ApiResponseError([], $e->getMessage(), 400);
        }
        try {
            // $user_id = auth('api')->user()->id;
            $loan_id = $request->loan_id;

            UserLoan::loanApplicationStatus($loan_id, 1);

            return $this->ApiResponseSuccess([], 'User loan application is approved.', 200);
        } Catch (\Exception $e) {
            return $this->ApiResponseError([], $e->getMessage(), 500);
        }
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @define User allowed for weekly repayments
     */
    
    public function userSubmitWeeklyRepayments(Request $request) {
        try {
            $request->validate([
                'loan_id' => 'required|numeric',
            ]);
        } catch (\Exception $e) {
            return $this->ApiResponseError([], $e->getMessage(), 400);
        }
        try {
            $loan_id = $request->loan_id;
            $responseData = [];

            $loan_details = UserLoan::checkLoanApplicationStatus($loan_id);
            if($loan_details->loan_status == 1) {
                $loan_weekly_payment_details = UserLoan::checkLoanWeeklyPaymentStatus($loan_id);
                if($loan_weekly_payment_details->loan_weekly_repayment == 0) {

                    $repayment_amount = UserLoan::loanWeeklyPaymentAmount($loan_weekly_payment_details->loan_amount, $loan_weekly_payment_details->loan_term);

                    UserLoanWeeklyPayment::storeWeekplyPaymentData($repayment_amount, $loan_id, $loan_weekly_payment_details->loan_term);

                    UserLoan::updateLoanWeeklyPaymentStatus($loan_id);
                }
                $responseData = UserLoanWeeklyPayment::getWeekplyPaymentData($loan_id);
            } else {
                return $this->ApiResponseSuccess([], 'Loan is not approved yet.', 200);    
            }

            return $this->ApiResponseSuccess($responseData, 'User is on weekely repayments.', 200);
        } Catch (\Exception $e) {
            return $this->ApiResponseError([], $e->getMessage(), 500);
        }
    }
}
