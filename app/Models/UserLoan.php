<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserLoan extends Model
{
    use HasFactory;

    protected $table = "user_loans";
    protected $primaryKey = "loan_id";
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'user_id',
        'loan_amount',
        'loan_term',
        'loan_status',
        'loan_weekly_repayment'
    ];

    public static function getStatusArr() {
        return ['0' => 'Pending', '1' => 'Accepted','2' =>'Rejected'];
    }

    public function userLoan() {
        return $this->belongsTo(User::class, 'user_id');
    }

    /*
     * Submit application for Loan
     */
    public static function applicationForLoan($user_id, $loan_amount, $loan_term) {
        self::updateOrCreate(
            [
                'user_id' => $user_id,
                'loan_status' => 0
            ],
            [
                'loan_amount' => $loan_amount,
                'loan_term' => $loan_term
            ]
        );
    }

    /*
     * Update loan application status (Approved/ Rejected)
     */
    public static function loanApplicationStatus($loan_id, $loan_status) {
        self::where('loan_id', $loan_id)->update(['loan_status' => $loan_status]);
    }

    /*
     * Check loan application status (Pending/ Approved/ Rejected)
     */
    public static function checkLoanApplicationStatus($loan_id) {
        return self::select('loan_status')->find($loan_id);
    }

    /*
     * Check loan weekly payment status (Pending/ Approved)
     */
    public static function checkLoanWeeklyPaymentStatus($loan_id) {
        return self::select('loan_weekly_repayment', 'loan_term', 'loan_amount')->find($loan_id);
    }

    /*
     * Update loan weekly payment status to Approved
     */
    public static function updateLoanWeeklyPaymentStatus($loan_id) {
        self::where('loan_id', $loan_id)->update(['loan_weekly_repayment' => 1]);
    }

    /*
     * Get loan weekly payment amount to be paid
     */
    public static function loanWeeklyPaymentAmount($loan_amount, $loan_term) {
        $annual_interest_rate = config('aspire.aspire_annual_interest_rate');
        $annual_weekely_terms = config('aspire.aspire_annual_weekely_terms');

        $params = array(
            'principal_amount' => $loan_amount,
            'loan_term' => $loan_term,
            'annual_interest_rate' => $annual_interest_rate,
            'annual_weekely_terms' => $annual_weekely_terms,
          );

        return self::calculateLoan($params);
    }

    /*
     * Math calculation to find laon calculation based on interest rate, loan term, loan amount and weekely terms
     */
    public static function calculateLoan($params) {
        $loan_amount = $params['principal_amount'];
        $loan_term = $params['loan_term'];
        $annual_interest_rate = $params['annual_interest_rate'];
        $annual_weekely_terms = $params['annual_weekely_terms'];

        $calc_perc = ($annual_interest_rate / $annual_weekely_terms) / 100;
        $interest_percentage = 1 + $calc_perc;	     
        $power =  $annual_weekely_terms * $loan_term;			 	
        $power_result = pow($interest_percentage, $power);

        $repayment_amount = round($loan_amount * ($power_result * $calc_perc) / ($power_result - 1), 2);

        return $repayment_amount;
    }
}
