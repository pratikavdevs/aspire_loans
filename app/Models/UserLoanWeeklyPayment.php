<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class UserLoanWeeklyPayment extends Model
{
    use HasFactory;

    protected $table = "user_loan_weekly_payments";

    protected $primaryKey = "weekely_payment_id";
    protected $fillable = [
        'loan_id',
        'weekly_payment',
        'weekly_payment_status',
        'scheduled_payment_date'
    ];

    public static function getStatusArr() {
        return ['0' => 'Scheduled', '1' => 'Paid'];
    }

    /*
     * Store all weekly repayments data with scheduled weekly payment date and what amount to be paid on that date
     */
    public static function storeWeekplyPaymentData($repayment_amount, $loan_id, $loan_term) {

        $duration = $loan_term * config('aspire.aspire_annual_weekely_terms');

        $repayment_data = array();
        for ($i = 1; $i <= $duration; $i++) {
            $days = $i * config('aspire.number_of_days_in_week');
            $repayment_data[] = array(
                                'loan_id' => $loan_id,
                                'weekly_payment' => $repayment_amount,
                                'scheduled_payment_date' => Carbon::now()->addDays($days),
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now()
                            );
        }

        self::insert($repayment_data);
    }

    /*
     * show all weekly repayments data with scheduled weekly payment date, what amount to be paid on that date with payment status
     */
    public static function getWeekplyPaymentData($loan_id) {
        return self::select('weekly_payment', 'weekly_payment_status', 'scheduled_payment_date')->where('loan_id', $loan_id)->get();
    }
}
