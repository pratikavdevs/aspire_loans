<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\UserLoanWeeklyPayment;
use Carbon\Carbon;

class LoanWeeklyRepayments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aspire:loan-weekly-repayments {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'All the loans will be assumed to have a weekly repayment frequency';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date = $this->argument('date');
        if($date) {
            $current_date = $date;
        } else {
            $current_date = Carbon::today()->format('Y-m-d');
        }

        UserLoanWeeklyPayment::where('scheduled_payment_date', $current_date)->update(['weekly_payment_status' => 1]);
    }
}
